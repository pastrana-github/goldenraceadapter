package co.com.goldenrace.tickect.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.goldenrace.tickect.dto.ResponseDTO;
import co.com.goldenrace.tickect.dto.TicketDTO;
import co.com.goldenrace.tickect.service.impl.TicketDBService;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("consulta")
public class TicketController {
	
	@Autowired
	private TicketDBService ticketDBService;	
	
	@GetMapping
	public ResponseEntity<Object> getTicket() {
		ResponseDTO responseDb = ticketDBService.getTicket();
		return new ResponseEntity<>(responseDb.getData(), HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<Object> addTicket(@RequestBody TicketDTO ticketDTO) {
		ResponseDTO responseDb = ticketDBService.addTicket(ticketDTO);
		return new ResponseEntity<>(responseDb.getData(), HttpStatus.OK);
	}

	@DeleteMapping
	public ResponseEntity<Object> deleteTicket(@RequestBody TicketDTO ticketDTO) {
		ResponseDTO responseDb = ticketDBService.deleteTicket(ticketDTO);
		return new ResponseEntity<>(responseDb.getData(), HttpStatus.OK);
	}
	
	@PutMapping
	public ResponseEntity<Object> updateTicket(@RequestBody TicketDTO ticketDTO) {
		ResponseDTO responseDb = ticketDBService.updateTicket(ticketDTO);
		return new ResponseEntity<>(responseDb.getData(), HttpStatus.OK);
	}

}
