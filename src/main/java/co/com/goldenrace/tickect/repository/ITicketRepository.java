package co.com.goldenrace.tickect.repository;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.com.goldenrace.tickect.entity.TicketEntity;

@Repository
@Transactional
public interface ITicketRepository extends CrudRepository<TicketEntity, Long> {

}
