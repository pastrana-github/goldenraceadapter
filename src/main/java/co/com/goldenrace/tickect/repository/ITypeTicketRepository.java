package co.com.goldenrace.tickect.repository;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.com.goldenrace.tickect.entity.Type_ticketEntity;

@Repository
@Transactional
public interface ITypeTicketRepository extends CrudRepository<Type_ticketEntity, Long> {

}
