package co.com.goldenrace.tickect.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseDTO implements Serializable{
		private static final long serialVersionUID = 1L;
		
		@JsonProperty(value = "Data")
		private Object data = new Object();

		public Object getData() {
			return data;
		}
		public void setData(Object data) {
			this.data = data;
		}
		public static long getSerialversionuid() {
			return serialVersionUID;
		}

		
}
