package co.com.goldenrace.tickect.dto;

import java.util.Map;

import org.springframework.http.HttpMethod;

public class MessageLogDTO {

	private String url;

	private HttpMethod httpMethod;

	private Map<String, Object> valores;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public HttpMethod getHttpMethod() {
		return httpMethod;
	}

	public void setHttpMethod(HttpMethod httpMethod) {
		this.httpMethod = httpMethod;
	}

	public Map<String, Object> getValores() {
		return valores;
	}

	public void setValores(Map<String, Object> valores) {
		this.valores = valores;
	}
}

