package co.com.goldenrace.tickect.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class StatusResponse
{
	private String statusDesc;

	private Integer status;

	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
	private Date timestamp;

	public String getStatusDesc()
	{
		return statusDesc;
	}

	public void setStatusDesc(String statusDesc)
	{
		this.statusDesc = statusDesc;
	}

	public Integer getStatus()
	{
		return status;
	}

	public void setStatus(Integer status)
	{
		this.status = status;
	}

	public Date getTimestamp()
	{
		return timestamp;
	}

	public void setTimestamp(Date timestamp)
	{
		this.timestamp = timestamp;
	}

	@Override
	public String toString()
	{
		return "StatusResponse [statusDesc=" + statusDesc + ", status=" + status + ", timestamp=" + timestamp + "]";
	}

}
