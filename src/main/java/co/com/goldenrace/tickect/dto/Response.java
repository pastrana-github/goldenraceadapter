package co.com.goldenrace.tickect.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class Response
{

	private StatusResponse statusResponse;

	@JsonInclude(Include.NON_NULL)
	private Object data;
	
	public StatusResponse getStatusResponse()
	{
		return statusResponse;
	}

	public void setStatusResponse(StatusResponse statusResponse)
	{
		this.statusResponse = statusResponse;
	}

	public Object getData()
	{
		return data;
	}

	public void setData(Object data)
	{
		this.data = data;
	}

	@Override
	public String toString()
	{
		return "PQRPNResponse [statusResponse=" + statusResponse + ", data=" + data + ", getStatusResponse()=" + getStatusResponse() + ", getData()=" + getData() + "]";
	}

}
