package co.com.goldenrace.tickect.dto;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class TicketDTO {
	
	@JsonProperty(value = "Id_ticket")
	private int id_ticket;
	
	@JsonProperty(value = "F_creacion")
	private LocalDate f_creacion;
	
	@JsonProperty(value = "Total")
	private int total;
	
	@JsonProperty(value = "Id_detalle")
	private Long id_detalle;
	
}
