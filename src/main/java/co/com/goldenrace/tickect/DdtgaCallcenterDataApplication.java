package co.com.goldenrace.tickect;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DdtgaCallcenterDataApplication {

	public static void main(String[] args) {
		SpringApplication.run(DdtgaCallcenterDataApplication.class, args);
	}

}
