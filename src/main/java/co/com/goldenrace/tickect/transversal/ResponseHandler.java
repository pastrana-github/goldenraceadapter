package co.com.goldenrace.tickect.transversal;

import co.com.goldenrace.tickect.dto.ResponseDTO;

public class ResponseHandler {

	public ResponseDTO response(Object dataRes, int status, String desc) {
		ResponseDTO response = new ResponseDTO();
		response.setData(dataRes);
		return response;
	}
}
