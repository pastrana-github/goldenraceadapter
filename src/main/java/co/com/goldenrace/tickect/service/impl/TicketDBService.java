package co.com.goldenrace.tickect.service.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import co.com.goldenrace.tickect.dto.ResponseDTO;
import co.com.goldenrace.tickect.dto.TicketDTO;
import co.com.goldenrace.tickect.entity.TicketEntity;
import co.com.goldenrace.tickect.repository.ITicketRepository;
import co.com.goldenrace.tickect.service.ITicketDBService;
import co.com.goldenrace.tickect.transversal.ResponseHandler;
@Service
public class TicketDBService implements ITicketDBService{
	
	@Autowired
	private ITicketRepository iTicketRepository;
	
	@Override
	public ResponseDTO getTicket() {
		List<TicketEntity> lisTicketEntity;
		List<TicketDTO> listTicketDTO = new ArrayList<TicketDTO>();
		ResponseDTO response = null;
		ResponseHandler responseHandler = new ResponseHandler();
		long lenghtNow = iTicketRepository.count();
		if (lenghtNow > 0) {
			try {
				lisTicketEntity = (List<TicketEntity>) iTicketRepository.findAll();				
				for (TicketEntity ticketEntity : lisTicketEntity) {
					TicketDTO ticketDTO = new TicketDTO();
					ticketDTO.setId_ticket(ticketEntity.getId_ticket());
					ticketDTO.setF_creacion(ticketEntity.getF_creacion());
					ticketDTO.setTotal(ticketEntity.getTotal());
					listTicketDTO.add(ticketDTO);
				}
				response = responseHandler.response(listTicketDTO, 200, "Registros encontrados: " + lisTicketEntity.size());
			} catch (DataAccessException e) {
				response = responseHandler.response(null, 500, "Error al listar los estados del WhatSapp");
			}
		}else {
			response = responseHandler.response(null, 200, "No se encontraron registros");
		}
		return response;
	}
	
	@Override
	public ResponseDTO addTicket(TicketDTO ticketDTO) {
		TicketEntity ticketEntity = new TicketEntity();
		ticketEntity.setF_creacion(LocalDate.now());
		ticketEntity.setTotal(ticketDTO.getTotal());
		ticketEntity.setId_detalle(ticketDTO.getId_detalle());		
		ResponseDTO response = null;
		ResponseHandler responseHandler = new ResponseHandler();
		try {
			long lenghtNow = iTicketRepository.count();
			iTicketRepository.save(ticketEntity);
			long lenghtAfter = iTicketRepository.count();
			if (lenghtAfter > lenghtNow) {
				response = responseHandler.response(ticketDTO, 201, "Agregado correctamente");
			} else {
				response = responseHandler.response(null, 204, "No se pudo agregar por violación de llaves compuestas");
			}
		} catch (DataAccessException e) {
			response = responseHandler.response(null, 500, "Error al guardar el registro");
		}
		return response;
	}
	
	@Override
	public ResponseDTO deleteTicket(TicketDTO ticketDTO) {
		TicketEntity ticketEntity = new TicketEntity();
		ticketEntity.setF_creacion(ticketDTO.getF_creacion());
		ticketEntity.setTotal(ticketDTO.getTotal());
		ticketEntity.setId_detalle(ticketDTO.getId_detalle());
		ResponseDTO response = null;
		ResponseHandler responseHandler = new ResponseHandler();
		try {
			long lenghtNow = iTicketRepository.count();
			iTicketRepository.delete(ticketEntity);
			long lenghtAfter = iTicketRepository.count();
			if (lenghtAfter < lenghtNow) {
				response = responseHandler.response(ticketDTO, 200, "Eliminado correctamente");
			} else {
				response = responseHandler.response(null, 204, "No se pudo eliminar");
			}
		} catch (DataAccessException e) {
			response = responseHandler.response(null, 500, "Error al eliminar el registro");
		}
		return response;
	}
	
	@Override
	public ResponseDTO updateTicket(TicketDTO ticketDTO) {		
		ResponseDTO response = null;
		ResponseHandler responseHandler = new ResponseHandler();
		List<TicketDTO> listTicketDTO = new ArrayList<TicketDTO>();
		List<TicketEntity> lisTicketEntity;		
		try {			
			if (!ticketDTO.equals(null)) {
				lisTicketEntity = (List<TicketEntity>) iTicketRepository.findAll();				
				for (TicketEntity ticketEntity : lisTicketEntity) {
					TicketDTO ticketDTONew = new TicketDTO();
					if(ticketDTO.getId_ticket() == ticketEntity.getId_ticket() ) {
						ticketDTONew.setF_creacion(LocalDate.now());
						ticketDTONew.setTotal(ticketEntity.getTotal());
						listTicketDTO.add(ticketDTONew);
					}					
				}
				response = responseHandler.response(ticketDTO, 200, "Eliminado correctamente");
			} else {
				response = responseHandler.response(null, 204, "No se pudo eliminar");
			}
		} catch (DataAccessException e) {
			response = responseHandler.response(null, 500, "Error al eliminar el registro");
		}
		return response;
	}

}
