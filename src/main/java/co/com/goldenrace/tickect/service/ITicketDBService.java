package co.com.goldenrace.tickect.service;

import co.com.goldenrace.tickect.dto.ResponseDTO;
import co.com.goldenrace.tickect.dto.TicketDTO;

public interface ITicketDBService {
	
	ResponseDTO getTicket();
	ResponseDTO addTicket(TicketDTO ticketDTO);
	ResponseDTO deleteTicket(TicketDTO ticketDTO);
	ResponseDTO updateTicket(TicketDTO ticketDTO);	
}
