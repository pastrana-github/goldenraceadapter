package co.com.goldenrace.tickect.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import java.time.LocalDate;

import lombok.Data;

@Data
@Table(name="TICKET")
@Entity
public class TicketEntity {
	
	@Id
	@Column(name="ID_TICKET")
	private int id_ticket;
	
	@Column(name = "F_CREACION", nullable = false)
	private LocalDate f_creacion;

	@Column(name="TOTAL")
	private int total;
	
	@Column(name="ID_DETALLE")
	private Long id_detalle;
	
}
