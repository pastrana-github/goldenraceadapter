package co.com.goldenrace.tickect.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
@Table(name="TIPO_TICKET")
@Entity
public class Type_ticketEntity {
	
	@Id
	@Column(name="ID_TICKET")
	private Long id_ticket;
	
	@Size(max = 100)
	@Column(name="DESCRIPCION")
	private String descripcion;
	
	@Column(name="CANTIDAD")
	private int cantidad;

}
